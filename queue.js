let collection = [];

// Write the queue functions below.
// Note: Avoid using Array methods (except .length) on creating the queue functions.

function print(){
	return collection;
};

function enqueue(add){
	collection[collection.length] = add;
	return collection;
}

function dequeue(){
	for(i = 1; i < collection.length; i++){
		collection[i - 1] = collection[i];

		collection.length--;
	}
	return collection;
}

function front(){
	let [first] = collection;

	return first;
}

function size(){
	return collection.length;
}

function isEmpty(){
	if(collection.length == 0){
		return true;
	}
	else{
		return false;
	}
}


module.exports = {
	//export created queue functions
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};